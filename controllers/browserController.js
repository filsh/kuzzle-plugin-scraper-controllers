const Promise = require('bluebird')
const amqplib = require('amqplib')
const request = require('amqplib-rpc').request

module.exports = function BrowserController (pluginContext, amqpConnection) {
  this.pluginContext = pluginContext
  this.amqpConnection = amqpConnection

  this.index = function (requestObject) {
    return this.amqpConnection.then((connection) => {
      return request(connection, 'phantomjs-queue', { a: 30, b: 20 }).then((replyMessage) => replyMessage.content.toString())
    }).then((msg) => new this.pluginContext.constructors.ResponseObject(requestObject, { msg: msg }))
  }
}
