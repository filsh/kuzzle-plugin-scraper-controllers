const amqplib = require('amqplib')
const request = require('amqplib-rpc').request
const BrowserController = require('../controllers/browserController')

module.exports = function () {
  this.controllers = require('../config/controllers')
  this.routes = require('../config/routes')
  this.pluginContext = null
  this.amqpConnection = null

  this.init = function (config, pluginContext, isDummy) {
    this.pluginContext = pluginContext

    if (!!config && config.amqp) {
      const amqpConfig = config.amqp
      amqpConfig.user = process.env.RABBITMQ_PHANTOMJS_USER
      amqpConfig.pass = process.env.RABBITMQ_PHANTOMJS_PASS

      this.amqpConnection = amqplib.connect(`amqp://${amqpConfig.user}:${amqpConfig.pass}@${amqpConfig.host}:${amqpConfig.port}/`)
        .catch(() => {
          console.error('----\n==>     ERROR: Unable connect to RabbitMQ.')
        })
    }
  }

  this.BrowserController = function () {
    return new BrowserController(this.pluginContext, this.amqpConnection)
  }
}
